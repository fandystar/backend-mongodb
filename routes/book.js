const express = require("express");
const router = express.Router();

const bookControllers = require("../controllers/book");

router.post("/add", bookControllers.Create);
router.get("/", bookControllers.Read);
router.put("/edit/:id", bookControllers.Update);
router.delete("/delete/:id", bookControllers.Delete);

module.exports = router;

